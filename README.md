# Magento Cookie Compliance
> magecomp_cookiecompliance-1.0.0

Magento Cookie Compliance extension by MageComp let store owner display consent cookie notification bar in your Magento store.


The EU is ready with it all new GDPR rule and set and up deadline to 25th may of 2018 and now the clock is ticking fast, you don't have enough time. To get your store ready for EU law, use this extension to notify your store visitors about cookie you are using and protect yourself from being penalized by EU.

In this last minutes, MageComp has come up with Magento Cookie Compliance extension that allows the store owner to display cookie consent notification in store frontend to notify store visitor that your store uses cookies and they must be agreed to store privacy policy before making any action. The extension is designed user-friendly and can be fully customized according to store theme. Also, the extension comes with the built-in integration of google analytics and facebook pixel to use. Using backend option, store admin can personalize cookie bar with their own message, text color and background color as per need.

Why choose MageComp’s Magento Cookie Compliance extension:
- Option to enable and disable extension from store backend.
- Backend option to add cookie content to display in store frontend.
- The extension features to enable use of facebook pixel and google analytics id as per need.
- Store admin can add custom title and link to privacy page of store.
- To personalized cookie bar store admin can change text color, background color and position as per need.

![](https://gitlab.com/magento1x/magecomp_cookiecompliance/-/raw/master/4_cookie_compliance_law_notification_on_bottom.webp)
![](https://gitlab.com/magento1x/magecomp_cookiecompliance/-/raw/master/1_configuration_8_2.webp)
